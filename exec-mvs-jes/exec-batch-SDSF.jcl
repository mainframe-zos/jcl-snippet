//JOB01    JOB (),'COMMAND',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* SDSF - EXECUTE MVS OR JES
//* **************************************************************
//STEP01   EXEC PGM=SDSF
//ISFOUT   DD SYSOUT=*
//DATAOUT  DD DSN=ps-dsname,
//            DISP=(,CATLG,DELETE),
//            DCB=(RECFM=FBA,LRECL=133),
//            SPACE=(CYL,(1,1)),UNIT=SYSDA
//ISFIN    DD *
  /D T
  /D J,L
  /$D SPL
  PRINT FILE DATAOUT
  ULOG
  PRINT
  PRINT CLOSE
/*
