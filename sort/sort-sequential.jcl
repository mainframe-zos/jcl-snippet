//JOB01    JOB (),'SORT PS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* SORT A PS
//* **************************************************************
//STEP01   EXEC PGM=SORT
//SORTIN   DD   DISP=SHR,DSN=ps-dsname
//SORTWK01 DD   UNIT=SYSDA,SPACE=(CYL,(1,1))
//SORTOUT  DD   DSN=ps-dsname,DISP=(,CATLG,DELETE),
//         DCB=(RECFM=FB,LRECL=80),UNIT=SYSDA,
//         SPACE=(TRK,(1,1),RLSE)
//SYSOUT   DD   SYSOUT=*
//SYSIN    DD   *
  SORT FIELDS=(1,6,CH,A,47,4,BI,A)
/*

//* **************************************************************
//* **************************************************************

//JOB01    JOB (),'SORT PS WITH COND',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* SORT A PS WITH MULTI COND
//* **************************************************************
//STEP01   EXEC PGM=SORT
//SORTIN   DD DISP=SHR,DSN=ps-in-dsname
//SORTOUT  DD DSN=ps-out-dsname,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(RECFM=FB,LRECL=50),
//            SPACE=(TRK,(5,2))
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
  INCLUDE COND=((11,7,CH,NE,C'RETRAIT'),AND, -
  (11,5,CH,NE,C'DEPOT'))
  SORT FIELDS=(22,10,CH,A)
/*

//* **************************************************************
//* **************************************************************

//JOB01    JOB (),'SORT MULTI PS WITH COND',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* SORT TWO PS WITH COND
//* **************************************************************
//STEP01   EXEC PGM=SORT
//SORTIN   DD DISP=SHR,DSN=ps1-in-dsname
//         DD DISP=SHR,DSN=ps2-in-dsname
//SORTOUT  DD DSN=ps-out-dsname,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(RECFM=FB,LRECL=50),
//            SPACE=(TRK,(5,2))
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
  INCLUDE COND=(1,3,CH,EQ,C'001')
  SORT FIELDS=(1,3,CH,A)
/*
