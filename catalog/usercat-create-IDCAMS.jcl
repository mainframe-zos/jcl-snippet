//JOB01    JOB (),'USERCAT CREATE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A USERCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
  DEFINE  USERCATALOG -
         (NAME(usercat-name) -
          CYL(5 1)             -
          CISZ(3584)           -
          VOL(volume-name)          -
          STRNO(30)            -
          BFND(5)              -
          BFNI(5)              -
          FSPC(15 15)          -
          RECSZ(4086 32400)    -
          ICFCAT)
//
