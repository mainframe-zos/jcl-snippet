//JOB01    JOB (),'ALIAS CREATE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A ALIAS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
    DEF ALIAS(REL(usercat-name)   NAME(alias-name))   -
    CAT(mastercat-name)
//