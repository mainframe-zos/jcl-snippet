//JOB01    JOB (),'MERGE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* MERGE TWO PDS TO ANOTHER PDS
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=*
//IN1      DD DSN=pds-dsname-in1,DISP=SHR
//IN2      DD DSN=pds-dsname-in2,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=OLD
//SYSIN    DD *
      COPY OUTDD=OUT
           INDD=IN1
           INDD=IN2
/*
