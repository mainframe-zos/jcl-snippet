//JOB01    JOB (),'REPLACE MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* REPLACE A MEMBER
//* **************************************************************
//STEP01   EXEC PGM=IEBUPDTE
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=pds-dsname-idem,DISP=SHR
//SYSUT2   DD DSN=pds-dsname-idem,DISP=SHR
//SYSIN    DD *
./        REPL NAME=member,LIST=ALL
* line1
* line2
/*
