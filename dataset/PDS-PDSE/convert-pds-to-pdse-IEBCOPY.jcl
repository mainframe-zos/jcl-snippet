//JOB01    JOB (),'CONVERT PDS PDSE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CONVERT A PDS TO A PDSE
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=pds-dsname,DISP=SHR
//SYSUT2   DD DSN=pdse-dsname,
//         DISP=(NEW,CATLG,DELETE),
//         DSNTYPE=LIBRARY,
//         DSORG=PO,
//         SPACE=(CYL,(10,10)),
//         DCB=(RECFM=FB,LRECL=80,BLKSIZE=800)
//SYSIN    DD *
   COPY INDD=SYSUT1,OUTDD=SYSUT2
/*
