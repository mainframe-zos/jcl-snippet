//JOB01    JOB (),'COPY PDSE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY A PDSE TO ANOTHER PDSE
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=*
//INFILE   DD DSN=pdse-dsname-in,DISP=OLD
//OUTFILE  DD DSN=pdse-dsname-out,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(CYL,(5,5)),UNIT=SYSDA,
//            DSNTYPE=LIBRARY,
//            DCB=(DSORG=PO,RECFM=FB,LRECL=80,BLKSIZE=32720)
//SYSIN    DD *
   COPY OUTDD=OUTFILE,INDD=INFILE
/*
