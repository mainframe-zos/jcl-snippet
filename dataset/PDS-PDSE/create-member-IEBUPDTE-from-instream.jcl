//JOB01    JOB (),'CREATE MEMBER INSTEAM',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A MEMBER WITH AN INSTEAM
//* **************************************************************
//STEP01   EXEC PGM=IEBUPDTE,PARM=NEW
//SYSPRINT DD SYSOUT=E
//SYSUT2   DD DSN=pds-dsname,DISP=OLD
//SYSIN    DD   DATA
./        ADD   NAME=member,LEVEL=01,SOURCE=0,LIST=ALL
./     NUMBER   NEW1=10,INCR=10
line1
line2
./      ENDUP
/*
