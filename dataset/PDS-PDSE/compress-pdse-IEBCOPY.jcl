//JOB01    JOB (),'COMPRESS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COMPRESS A PDSE
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=E
//IN       DD DSN=pdse-dsname-idem,DISP=OLD
//OUT      DD DSN=pdse-dsname-idem,DISP=OLD
//         LIKE=pdse-dsname-idem
//SYSIN    DD *
   COPY INDD=IN,OUTDD=OUT
/*
