//JOB01    JOB (),'DELETE MEMBER',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE A MEMBER TO A PDS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//PDS      DD DISP=SHR,DSN=pds-dsname
//SYSPRINT DD SYSOUT=E
//SYSIN    DD    *
     DELETE -
   pds-dsname(member)    FILE(PDS)
/*
