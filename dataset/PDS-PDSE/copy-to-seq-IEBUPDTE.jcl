//JOB01    JOB (),'COPY MEMBER TO PS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY ONE MEMBER TO A PS
//* **************************************************************
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=pds-dsname,DISP=SHR
//SYSUT2   DD DSN=ps-dsname,DISP=(,CATLG),UNIT=SYSDA,
//         SPACE=(TRK,(5,5,3)),DCB=(RECFM=FB,LRECL=80,BLKSIZE=4080)
//SYSIN    DD *
./      REPRO NEW=PS,MEMBER=member
/*
