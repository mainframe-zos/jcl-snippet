//JOB01    JOB (),'CHANGE MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CHANGE : UPDATE A MEMBER
//* **************************************************************
//STEP01   EXEC PGM=IEBUPDTE
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=pds-dsname,DISP=SHR
//*
//SYSIN    DD *
./      CHANGE NAME=member,UPDATE=INPLACE,LIST=ALL
* ANYNET2          WDL2JJEK                                             00000010
/*
