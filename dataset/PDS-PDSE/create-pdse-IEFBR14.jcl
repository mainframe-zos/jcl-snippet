//JOB01    JOB (),'CREATE PDSE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A PDSE
//* **************************************************************
//STEP01   EXEC PGM=IEFBR14
//DD1      DD DSN=pdse-dsname,
//         DISP=(NEW,CATLG,DELETE),
//         DSNTYPE=LIBRARY,
//         UNIT=SYSALLDA,
//         SPACE=(CYL,(10,10,10)),
//         DCB=(RECFM=FB,LRECL=80,BLKSIZE=800)
//
