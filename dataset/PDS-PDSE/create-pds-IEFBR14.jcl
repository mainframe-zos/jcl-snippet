//JOB01    JOB (),'CREATE PDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A PDS
//* **************************************************************
//STEP01   EXEC PGM=IEFBR14
//DD1      DD DSN=pds-dsname,
//            DISP=(NEW,CATLG,DELETE),
//            DSNTYPE=PDS,
//            SPACE=(TRK,(45,15,40),RLSE),
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=800)
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
