//JOB01    JOB (),'REFORMAT',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* REFORMAT MEMBERS
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=pds-dsname-in,DISP=OLD
//SYSUT2   DD DSN=pds-dsname-out,DISP=(,CATLG),UNIT=SYSDA,
//         SPACE=(TRK,(5,2,2))
//SYSIN    DD *
       GENERATE MAXNAME=2,MAXGPS=2
       MEMBER   NAME=member1
GROUP1 RECORD   IDENT=(8,'value1',1)
       MEMBER   NAME=merber2
GROUP2 RECORD   IDENT=(6,'value2',1)
/*
