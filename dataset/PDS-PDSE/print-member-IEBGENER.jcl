//JOB01    JOB (),'PRINT MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* PRINT A MEMBER
//* **************************************************************
//STEP04   EXEC PGM=IEBGENER
//SYSIN    DD DUMMY
//SYSPRINT DD SYSOUT=E
//SYSUT1   DD DISP=SHR,DSN=pds-dsname(member)
//SYSUT2   DD SYSOUT=E,HOLD=NO
