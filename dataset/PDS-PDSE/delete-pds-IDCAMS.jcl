//JOB01    JOB (),'DELETE PDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE A PDS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//PDS      DD DISP=SHR,DSN=pds-dsname
//SYSPRINT DD SYSOUT=E
//SYSIN    DD    *
     DELETE -
   pds-dsname    FILE(PDS)
/*
