//JOB01    JOB (),'COPY EXCLUDE MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//*
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=E
//IN       DD DSN=pds-dsname-in,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=(NEW,CATLG,DELETE),
//         LIKE=pds-dsname-in
//SYSIN    DD *
      COPY OUTDD=OUT
           INDD=IN
           EXCLUDE MEMBER=(member1,member2)
/*
