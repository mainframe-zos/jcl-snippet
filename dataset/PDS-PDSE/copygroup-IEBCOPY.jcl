//JOB01    JOB (),'COPYGROUP',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* EXEMPLE OF COPYGROUP USAGE
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=*
//IN       DD DSN=pds-dsname-in,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=OLD
//SYSIN    DD *
 COPYGROUP INDD=IN,OUTDD=OUT
     SELECT MEMBER=(DSN*)
/*
