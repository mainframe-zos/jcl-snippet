//JOB01    JOB (),'COPY ALL MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY ALL MEMBERS TO A PDS TO ANOTHER PDS
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=E
//IN       DD DSN=pds-dsname-in,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=(NEW,CATLG,DELETE),
//         LIKE=pds-dsname-in
//SYSIN    DD *
   COPY INDD=IN,OUTDD=OUT
/*

//* **************************************************************

//JOB01    JOB (),'COPY ALL MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY ALL MEMBERS TO A PDS TO A ANOTHER PDS WITH REPLACE
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=E
//IN       DD DSN=pds-dsname-in,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=(NEW,CATLG,DELETE),
//         LIKE=pds-dsname-in
//SYSIN    DD *
   COPY INDD=((IN,R)),OUTDD=OUT
/*


//* **************************************************************

//JOB01    JOB (),'COPY ALL MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY ALL MEMBERS TO A PDS TO A ANOTHER PDS WITH EXCLUDE
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=E
//IN       DD DSN=pds-dsname-in,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=(NEW,CATLG,DELETE),
//         LIKE=pds-dsname-in
//SYSIN    DD *
      COPY OUTDD=OUT
           INDD=IN
           EXCLUDE MEMBER=(member1,member2)
/*

//* **************************************************************

//JOB01    JOB (),'COPY SELECT MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY SELECTED MEMBERS TO A PDS TO A ANOTHER PDS
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=E
//IN       DD DSN=pds-dsname-in,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=(NEW,CATLG,DELETE),
//         LIKE=pds-dsname-in
//SYSIN    DD *
      COPY OUTDD=OUT
           INDD=IN
           SELECT MEMBER=(member1,member2)
/*

//* **************************************************************

//JOB01    JOB (),'COPY SELECT MEMBER WITH SYSUT3',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY SELECTED MEMBERS TO A PDS TO A ANOTHER PDS WITH SYSUT3
//* **************************************************************
//STEP01   EXEC PGM=IEBCOPY
//SYSPRINT DD SYSOUT=*
//IN       DD DSN=pds-dsname-in,DISP=SHR
//OUT      DD DSN=pds-dsname-out,DISP=(NEW,CATLG),
//         SPACE=(TRK,(5,2,5)),
//         UNIT=SYSDA
//SYSUT3   DD UNIT=SYSDA,SPACE=(TRK,(5,2))
//SYSIN    DD *
COPYOPER  COPY INDD=IN,OUTDD=OUT
 SELECT MEMBER=member1
 SELECT MEMBER=member2
 SELECT MEMBER=member3
/*
