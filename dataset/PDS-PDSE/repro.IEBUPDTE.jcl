//JOB01    JOB (),'REPRO MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* REPRO A MEMBER TO ANOTHER
//* **************************************************************
//STEP01   EXEC PGM=IEBUPDTE
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=pds-dsname-in,DISP=SHR
//SYSUT2   DD DSN=pds-dsname-out,DISP=OLD
//SYSIN    DD *
./       REPRO NAME=member,LEVEL=01,SOURCE=0,LIST=ALL
/*
