//JOB01    JOB (),'DELETE PDSE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE A PDSE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD    *
   DELETE ('pdse-dsname')
   IF LASTCC = 8 THEN -
     SET MAXCC = 0
/*
