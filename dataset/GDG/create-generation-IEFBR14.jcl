//JOB01    JOB (),'CREATE GENERATION',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE GDG GENERATION (PDS) + LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IEFBR14
//DD1      DD   DSN=gdg-dsname(+1),DISP=(NEW,CATLG,DELETE),
//         SPACE=(10,(2,1,1)),AVGREC=U
//DD2      DD   DSN=gdg-dsname(+2),DISP=(NEW,CATLG,DELETE),
//         SPACE=(10,(2,1,1)),AVGREC=U
//DD3      DD   DSN=gdg-dsname(+3),DISP=(NEW,CATLG,DELETE),
//         SPACE=(10,(2,1,1)),AVGREC=U
//DD4      DD   DSN=gdg-dsname(+4),DISP=(NEW,CATLG,DELETE),
//         SPACE=(10,(2,1,1)),AVGREC=U
//DD5      DD   DSN=gdg-dsname(+5),DISP=(NEW,CATLG,DELETE),
//         SPACE=(10,(2,1,1)),AVGREC=U
//*
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  LISTC LVL(gdg-dsname) ALL
//*
