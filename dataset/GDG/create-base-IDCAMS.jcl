//JOB01    JOB (),'CREATE GDG',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE GDG BASE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
  DEFINE GDG(NAME(gdg-dsname) -
    LIMIT(15) -
    NOEMPTY -
    LIFO -
    SCRATCH )
/*


//* **************************************************************
//* **************************************************************


//JOB01    JOB (),'CREATE GDG',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE GDG BASE + MODEL (PS)
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
          DEFINE GDG(NAME(gdg-dsname)          -
                 LIMIT(3)                               -
                 NOEMPTY                                -
                 NOSCRATCH)
/*
//STEP02   EXEC PGM=IEFBR14
//GDGMODEL DD  DSN=gdg-dsname.MODEL,
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=3390,
//             SPACE=(TRK,1),
//             DCB=(DSORG=PS,LRECL=80,RECFM=FB,BLKSIZE=0)
//


//* **************************************************************
//* **************************************************************


//JOB01    JOB (),'CREATE GDG',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE + CREATE + LISTCAT GDG BASE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
 DELETE gdg-dsname.*
 DELETE gdg-dsname
 SET MAXCC = 0
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
     DEFINE GDG                 -
       (NAME(gdg-dsname) -
        SCR NEMP LIMIT(3))
//*
//STEP03   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  LISTC LVL(gdg-dsname) ALL
//*
