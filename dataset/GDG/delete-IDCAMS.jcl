//JOB01    JOB (),'DELETE GDG',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE GDG BASE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 DELETE gdg-dsname -
        GENERATIONDATAGROUP -
        FORCE
/*

//* **************************************************************
//* **************************************************************


//JOB01    JOB (),'DELETE GDG',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE GDG BASE + MODEL
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DELETE ('gdg-dsname') -
      GDG FORCE PURGE
   DELETE ('gdg-dsname.MODEL')
  IF LASTCC = 8 THEN -
     SET MAXCC = 0
/*
