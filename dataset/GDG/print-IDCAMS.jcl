//JOB01    JOB (),'PRINT GDG',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* PRINT GDG
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=B
//INDD0    DD DISP=OLD,DSN=gdg-dsname(+0)
//INDDM1   DD DISP=OLD,DSN=gdg-dsname(-1)
//SYSIN    DD *
    PRINT INFILE(INDD0) CHARACTER
    PRINT INFILE(INDDM1) CHARACTER
/*
