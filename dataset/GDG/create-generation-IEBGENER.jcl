//JOB01    JOB (),'CREATE GENERATION',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE GDG GENERATION WITH MODEL
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSPRINT DD  SYSOUT=E
//SYSIN    DD  DUMMY
//SYSUT1   DD  *
DATA IN GENERATION GDG
SOME OTHER DATA
/*
//SYSUT2   DD  DSN=gdg-dsname(+1),
//             DISP=(NEW,CATLG,DELETE),
//             UNIT=3390,
//             SPACE=(TRK,1),
//             DCB=(gdg-dsname.MODEL)
