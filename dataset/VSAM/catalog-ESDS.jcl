//JOB01    JOB (),'CATALOG ESDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CATALOG ESDS + LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
    DEFINE CLUSTER(                     -
             NAME(esds-dsname) -
             VOLUMES(FSMS11)            -
             NONINDEXED                 -
             RECATALOG)
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG ESDS       */
  LISTCAT ENT(esds-dsname) ALL
//*
