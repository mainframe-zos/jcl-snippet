//JOB01    JOB (),'CREATE ESDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE ESDS VSAM
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DELETE SYSAR.VSAM.ESDS CLUSTER
   DEFINE CLUSTER (NAME(esds-dsname)           -
   NONINDEXED                              -
   RECSZ(80 80)                            -
   TRACKS(1,1)                             -
   CISZ(4096)                              -
   FREESPACE(10 5) )                       -
   DATA (NAME(esds-dsname.DATA))
/*

//* **************************************************************

//JOB01    JOB (),'CREATE ESDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE ESDS VSAM AND LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE esds-dsname
  SET MAXCC = 0

  DEFINE CLUSTER                       -
           (NAME(esds-dsname)          -
           NONINDEXED                  -
           CYL(2 1)                    -
           VOLUMES(SMSVOL)             -
           RECSZ(80 80)                -
           OWNER(FORE))
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG ESDS       */
  LISTCAT ENT(esds-dsname) ALL
//*
