//JOB01    JOB (),'CREATE RRDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE RRDS VSAM AND LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE rrds-dsname
  SET MAXCC = 0

  DEFINE CLUSTER                       -
           (NAME(rrds-dsname)          -
           NUMBERED                    -
           CYL(2 1)                    -
           VOLUMES(SMSVOL)             -
           RECSZ(80 80)                -
           OWNER(FORE))
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG RRDS       */
  LISTCAT ENT(rrds-dsname) ALL
//*
