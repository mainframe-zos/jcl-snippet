//JOB01    JOB (),'LISTCAT AIX',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* LISTCAT OF A AIX
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   LISTCAT ENTRY(my.aix) -
   CLUSTER DATA INDEX ALL
/*

//* **************************************************************

//JOB01    JOB (),'LISTCAT AIX PATH',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* LISTCAT OF A PATH
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   LISTCAT ENTRY(my.aix.PATH) -
   CLUSTER DATA INDEX ALL
/*
