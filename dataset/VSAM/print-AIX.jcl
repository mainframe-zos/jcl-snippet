//JOB01    JOB (),'PRINT AIX',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* PRINT A AIX
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//INPUT    DD DSN=my.aix.PATH,DISP=SHR
//SYSIN    DD *
   PRINT              -
        INFILE(INPUT) -
        CHAR
/*
