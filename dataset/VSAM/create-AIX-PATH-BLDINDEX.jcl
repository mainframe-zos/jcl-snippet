//JOB01    JOB (),'CREATE AIX PATH AND BUILD INDEX',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE AN AIX + THE PATH AND CALL BUILD INDEX
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DEFINE AIX (NAME(my.aix) -
   RELATE(my.ksds)              -
   TRACKS(15,1)                     -
   CISZ(4096)                       -
   FREESPACE(20 20)                 -
   KEYS(10 6)                       -
   NONUNIQUEKEY                     -
   UPGRADE                          -
   RECSZ(80 80))                    -
   DATA(NAME(my.aix.DATA))  -
   INDEX(NAME(my.aix.INDEX))
/*
//* CREATE PATH
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DEFINE PATH                   -
      (NAME(my.aix.PATH) -
      PATHENTRY(my.aix))
/*
//* BUILD INDEX
//STEP03   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   BLDINDEX                   -
   INDATASET(my.ksds)     -
   OUTDATASET(my.aix)
/*
