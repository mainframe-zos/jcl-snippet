//JOB01    JOB (),'CREATE PATH',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DEFINE PATH AIX
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DEFINE PATH                          -
       (NAME(aix-dsname.PATH)    -
       PATHENTRY(aix-dsname))
/*

//* **************************************************************

//JOB01    JOB (),'CREATE PATH',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//*    DEFINE PATH AIX AND LISTCAT AND PRINT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE aix-dsname.PATH
  SET MAXCC = 0

  DEFINE PATH                            -
         (NAME(aix-dsname.PATH)     -
         PATHENTRY(aix-dsname))
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG PATH       */
  LISTCAT ENT(aix-dsname.PATH) ALL
//*
//STEP03   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  PRINT IDS(aix-dsname.PATH) CHAR
//*
