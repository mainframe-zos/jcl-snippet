//JOB01    JOB (),'KSDS ALTER FREESPACE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* ALTER FREESPACE CI CA OF A KSDS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
        ALTER  my.ksds.DATA -
        FREESPACE(6 6)
/*
