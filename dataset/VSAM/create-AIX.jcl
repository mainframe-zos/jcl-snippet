//JOB01    JOB (),'CREATE AIX',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DEFINE ALTERNATE INDEX (AIX) WITH DELETE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
        DELETE my.aix AIX
        IF LASTCC = 8 THEN -
           SET MAXCC = 0
        DEFINE AIX (NAME(my.aix)  -
        RELATE(my.ksds)                -
        TRACKS(1,1)                            -
        CISZ(4096)                             -
        FREESPACE(10,10)                       -
        KEYS(26,20)                            -
        NONUNIQUEKEY                           -
        UPGRADE                                -
        RECORDSIZE(160,160))                   -
        DATA(NAME(my.aix.DATA))   -
        INDEX(NAME(my.aix.INDEX))
/*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
       BLDINDEX                          -
       INDATASET(my.ksds)        -
       OUTDATASET(my.aix)
/*

//* **************************************************************

//JOB01    JOB (),'CREATE AIX',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DEFINE ALTERNATE INDEX (AIX) WITH DELETE
//* AND LISTCAT AND PRINT
//* **************************************************************
//*
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE aix-dsname
  SET MAXCC = 0

  DEFINE ALTERNATEINDEX                  -
           (NAME(aix-dsname)    -
           RELATE(ksds-dsname)  -
           TRK(1 1)                      -
           VOLUMES(SMSVOL)               -
           KEYS(8 50)                    -
           RECSZ(80 80)                  -
           OWNER(FORE))
//*
//STEP02   EXEC PGM=IDCAMS
//KSDSDD   DD   DSN=ksds-dsname,DISP=OLD
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  BLDINDEX INFILE(KSDSDD)         -
           OUTDATASET(aix-dsname)
//*
//STEP03   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LISTE CATALOG AIX        */
  LISTCAT ENT(aix-dsname) ALL
//*
//STEP04   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  PRINT IDS(aix-dsname) CHAR
//*
