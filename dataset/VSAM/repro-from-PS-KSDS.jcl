//JOB01    JOB (),'REPRO FROM PS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* REPRO FROM PS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//IN01     DD DSN=my.ps,DISP=SHR
//OUT01    DD DSN=my.ksds,DISP=OLD
//SYSIN    DD *
   REPRO INFILE(IN01) OUTFILE(OUT01)
/*

//* **************************************************************

//JOB01    JOB (),'REPRO FROM PS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* REPRO FROM PS WITH REPLACE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//IN01     DD DSN=my.ps,DISP=SHR
//OUT01    DD DSN=my.ksds,DISP=OLD
//SYSIN    DD *
   REPRO INFILE(IN01) OUTFILE(OUT01) REPLACE
/*
