//JOB01    JOB (),'PRINT',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* PRINT A ESDS AND A KSDS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//DDIN1    DD DSN=my.esds,DISP=SHR
//DDIN2    DD DSN=my.ksds,DISP=SHR
//SYSIN    DD *
   PRINT INFILE(DDIN1) CHAR
   PRINT INFILE(DDIN2) CHAR
/*
