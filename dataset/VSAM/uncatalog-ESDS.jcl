//JOB01    JOB (),'UNCATALOG ESDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* UNCATALOG ESDS AND LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE esds-dsname NOERASE NOSCRATCH
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG ESDS       */
  LISTCAT ENT(esds-dsname) ALL
//*
