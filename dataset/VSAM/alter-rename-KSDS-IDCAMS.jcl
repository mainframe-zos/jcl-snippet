//JOB01    JOB (),'ALTER RENAME KSDS AND COMPOSANTS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* RENAME VSAM FILE AND ALL COMPONENTS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
    ALTER -
      my-ksds -
      NEWNAME(my-ksds-newname)
    ALTER -
      my-ksds.* -
      NEWNAME(my-ksds-newname.*)
/*


//* **************************************************************
//* **************************************************************


//JOB01    JOB (),'CREATE AND RENAME KSDS AND COMPOSANTS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID

//* **************************************************************
//* RENAME VSAM FILE AND ALL COMPONENTS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=A
//SYSIN    DD   *
    DELETE my.ksds CLUSTER
    IF LASTCC = 8 THEN -
       SET MAXCC = 0
    DEFINE CLUSTER -
           (NAME(my.ksds) -
           TRK(1 1)) -
           DATA -
             (NAME(my.ksds.DATA)) -
           INDEX -
             (NAME(my.ksds.INDEX))
           ALTER -
                  my.ksds -
                  NEWNAME(my.new.ksds)
           ALTER -
                  my.ksds.* -
                  NEWNAME(my.new.ksds.*)
/*
