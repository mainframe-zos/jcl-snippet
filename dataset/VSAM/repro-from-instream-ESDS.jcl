//JOB01    JOB (),'LISTCAT AIX',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* LOAD DATA WITHIN A ESDS FROM INSTREAM
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS,REGION=0M
//SYSPRINT DD  SYSOUT=*
//DDOUT    DD  DSN=my.esds,DISP=SHR
//DDIN     DD  *
05data   OTHERDATA
06data   OTHERDATA
02data   OTHERDATA
04data   OTHERDATA
03data   OTHERDATA
/*
//SYSIN    DD  *
 REPRO INFILE(DDIN)-
       OUTFILE(DDOUT)
/*
