//JOB01    JOB (),'DELETE KSDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE A KSDS CLUSTER
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DELETE ('my.ksds') CLUSTER
  IF LASTCC = 8 THEN -
     SET MAXCC = 0
/*

//* **************************************************************

//JOB01    JOB (),'DELETE KSDS WITH OTHER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE ALL KSDS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DELETE ('my.ksds')
  IF LASTCC = 8 THEN -
     SET MAXCC = 0
/*
