//JOB01    JOB (),'REPRO FROM PDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* LOAD KSDS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//IN01     DD DSN=pds-dsname(member),DISP=SHR
//OUT01    DD DSN=ksds-dsname,DISP=OLD
//SYSIN    DD *
   REPRO INFILE(IN01) OUTFILE(OUT01)
/*

//* **************************************************************

//JOB01    JOB (),'REPRO FROM PDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//*
//* **************************************************************
//* LOAD KSDS WITH REPLACE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//IN01     DD DSN=pds-dsname(member),DISP=SHR
//OUT01    DD DSN=ksds-dsname,DISP=OLD
//SYSIN    DD *
   REPRO INFILE(IN01) OUTFILE(OUT01) REPLACE
/*

//* **************************************************************

//JOB01    JOB (),'REPRO FROM PDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* LOAD KSDS AND LISTCAT AND PRINT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//STEP01   DD   DISP=SHR,DSN=pds-dsname(member)
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  REPRO INFILE(INPUT)    -
        OUTDATASET(ksds-dsname)
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG KSDS       */
  LISTCAT ENT(ksds-dsname) ALL
//*
//STEP03   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  PRINT IDS(ksds-dsname) CHAR
//*
