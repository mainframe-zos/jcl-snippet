//JOB01    JOB (),'CREATE KSDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE KSDS VSAM
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
   DEFINE CLUSTER (NAME(ksds-dsname)       -
   INDEXED                                 -
   KEYS(6 0)	                            -
   RECSZ(80 80)                            -
   TRACKS(15,1)                            -
   CISZ(4096)                              -
   FREESPACE(3 3) )                        -
   DATA (NAME(ksds-dsname.DATA))           -
   INDEX (NAME(ksds-dsname.INDEX))
/*

//* **************************************************************

//JOB01    JOB (),'CREATE KSDS DELETE BEFORE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE KSDS VSAM WITH A DELETE BEFORE
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
        DELETE my.ksds CLUSTER
        IF LASTCC = 8 THEN -
           SET MAXCC = 0
        DEFINE CLUSTER (NAME(my.ksds)      -
        INDEXED                                     -
        RECSZ(160 160)                                -
        TRACKS(1,1)                                 -
        KEYS(5  0)                                  -
        CISZ(4096)                                  -
        FREESPACE(3 3) )                            -
        DATA (NAME(my.ksds.DATA))              -
        INDEX (NAME(my.ksds.INDEX))
/*

//* **************************************************************

//JOB01    JOB (),'CREATE KSDS DELETE BEFORE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE KSDS VSAM WITH A DELETE BEFORE AND LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE ksds-dsname
  SET MAXCC = 0

  DEFINE CLUSTER                       -
           (NAME(ksds-dsname) -
           CYL(2 1)                    -
           VOLUMES(SMSVOL)             -
           KEYS(6 0)                   -
           RECSZ(80 80)                -
           OWNER(FORE))
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG KSDS       */
  LISTCAT ENT(ksds-dsname) ALL
//*
