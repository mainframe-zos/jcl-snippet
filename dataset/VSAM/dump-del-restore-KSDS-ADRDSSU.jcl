//JOB01    JOB (),'DUMP DEL RESTORE KSDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* ADRDSSU
//* **************************************************************
//STEP01   EXEC PGM=ADRDSSU
//SYSPRINT DD SYSOUT=E
//DUMP     DD DSN=my-dump,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1))
//SYSIN    DD *
 /* DUMP AND DELETE */
 DUMP OUTDDNAME(DUMP)             -
 DATASET(INCLUDE(my-ksds)) -
 DELETE

 /* RESTORE */
 RESTORE INDDNAME(DUMP)           -
 DATASET(INCLUDE(my-ksds)) -
 CATALOG                          -
 REPLACE
/*
