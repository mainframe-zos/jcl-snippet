//JOB01    JOB (),'CREATE LINEAR',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE LINEAR VSAM AND LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE linear-dsname
  SET MAXCC = 0

  DEFINE CLUSTER                      -
           (NAME(linear-dsname)       -
           LINEAR                     -
           CYL(2 1)                   -
           VOLUMES(SMSVOL)            -
           OWNER(FORE))
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG LDS        */
  LISTCAT ENT(linear-dsname) ALL
