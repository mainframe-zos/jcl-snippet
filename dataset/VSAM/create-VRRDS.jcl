//JOB01    JOB (),'CREATE RRDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE VRRDS VSAM AND LISTCAT
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  DELETE vrrds-dsname
  SET MAXCC = 0

  DEFINE CLUSTER                        -
           (NAME(vrrds-dsname)          -
           NUMBERED                     -
           CYL(2 1)                     -
           VOLUMES(SMSVOL)              -
           RECSZ(60 80)                 -
           OWNER(FORE))
//*
//STEP02   EXEC PGM=IDCAMS
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  /* LIST CATALOG VRRDS      */
  LISTCAT ENT(vrrds-dsname) ALL
//*
