//JOB01    JOB (),'EXAMINE KSDS',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),REGION=2048K,
//         NOTIFY=&SYSUID
//* **************************************************************
//* EXAMINE KSDS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=*
//SYSIN    DD  *
  EXAMINE                      -
  NAME(ksds-name)              -
  DATATEST INDEXTEST
/*
