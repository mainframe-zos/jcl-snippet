//JOB01    JOB (),'CONCATENATE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CONCATENATE SOME PS TO ANOTHER PS
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=ps-dsname-in1,DISP=OLD
//         DD DSN=ps-dsname-in2,DISP=OLD
//         DD DSN=ps-dsname-in3,DISP=OLD
//SYSUT2   DD DSN=ps-dsname-out,DISP=(,CATLG),
//         UNIT=TAPE
//SYSIN    DD DUMMY
