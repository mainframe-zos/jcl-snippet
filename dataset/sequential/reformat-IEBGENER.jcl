//JOB01    JOB (),'REFORMAT',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* REFORMAT A PS TO ANOTHER PS
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=ps-dsname,DISP=OLD
//SYSUT2   DD DSN=ps-dsname,DISP=(,CATLG),UNIT=SYSDA,
//         DCB=(DSORG=PS,RECFM=FB,LRECL=52),
//         SPACE=(TRK,(10,5))
//SYSIN    DD *
     GENERATE MAXFLDS=99
     RECORD   FIELD=(20,1,,1),
              FIELD=(20,21,,21),
              FIELD=(8,41,ZP,41),
              FIELD=(7,49,,46)
/*
