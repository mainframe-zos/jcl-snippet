//JOB01    JOB (),'CREATE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A PS
//* **************************************************************
//STEP02   EXEC PGM=IEFBR14
//DD1      DD DSN=ps-dsname,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1)),
//            DCB=(RECFM=FB,LRECL=80,DSORG=PS)
