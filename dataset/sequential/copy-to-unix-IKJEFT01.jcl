//JOB01    JOB (),'COPY TO UNIX',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* TO A PS TO A UNIX FILE
//* **************************************************************
//STEP01   EXEC PGM=IKJEFT01
//SYSUT1   DD DSN=ps-dsname,
//         DISP=SHR
//SYSUT2   DD PATH='/my/filename',
//         PATHOPTS=(OWRONLY,OCREAT,OAPPEND),
//         PATHMODE=(SIWUSR)
//SYSTSPRT DD SYSOUT=*
//SYSTSIN  DD *
 OCOPY INDD(SYSUT1) OUTDD(SYSUT2)
/*
