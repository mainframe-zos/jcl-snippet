//JOB01    JOB (),'DELETE WITH LISTC',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE A PS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS,REGION=0M
//SYSPRINT DD  DSN=
//SYSIN    DD  *
   LISTC ENT('ps-dsname')
   IF LASTCC = 0 THEN                      -
     DELETE                                -
           ps-dsname
   ELSE SET MAXCC = 0
/*
