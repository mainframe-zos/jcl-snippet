//JOB01    JOB (),'COPY',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY A PS
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSIN    DD DUMMY
//SYSPRINT DD SYSOUT=E
//SYSUT1   DD DSN=ps-dsname,DISP=SHR
//SYSUT2   DD DSN=ps-dsname,
//            DISP=(NEW,CATLG,DELETE)
//            UNIT=3390,SPACE=(TRK,(1,1))
