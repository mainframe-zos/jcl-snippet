//JOB01    JOB (),'CALLOCATE PS QSAM',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* ALLOCATE PS QSAM
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD  SYSOUT=A
//SYSIN    DD  *
    ALLOC                                -
           DSNAME('ps-dsname')           -
           NEW  CATALOG                  -
           SPACE(3,2)  TRACKS            -
           BLKSIZE(1000)                 -
           LRECL(100)                    -
           DSORG(PS)                     -
           UNIT(3390)                    -
           RECFM(F,B)
/*
