//JOB01    JOB (),'DUMP MULTI PS TO GDG',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* ADRDSSU DUMP
//* **************************************************************
//STEP01   EXEC PGM=ADRDSSU
//SYSPRINT DD SYSOUT=*
//OUTFILE  DD DSN=gdg-dsname(+1),
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(1,1)),
//            LIKE=ps1-dsname
//SYSIN    DD *
 DUMP OUTDDNAME(OUTFILE)            -
 DATASET(INCLUDE(ps1-dsname         -
                 ps2-dsname         -
                )                   -
        )
/*
