//JOB01    JOB (),'PRINT',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* PRINT A PS
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSIN    DD DUMMY
//SYSPRINT DD SYSOUT=E,HOLD=NO
//SYSUT1   DD DSN=ps-dsname,DISP=SHR
//SYSUT2   DD SYSOUT=E,HOLD=NO
