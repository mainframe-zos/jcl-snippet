//JOB01    JOB (),'CREATE FROM INSTEAM',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE PS FROM A INSTREAM
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSUT2   DD DSN=ps-dsname,
//            DISP=(NEW,CATLG,DELETE),
//            SPACE=(TRK,(5,2)),
//            UNIT=3390
//            DCB=(DSORG=PS,RECFM=FB,LRECL=80)
//SYSIN    DUMMY
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD *
line 1
line2
...
line
$$
/*
