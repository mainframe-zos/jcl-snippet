//JOB01    JOB (),'CREATE INSTEAM',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE PS FROM A INSTREAM
//* **************************************************************
//STEP01   EXEC PGM=IEBUPDTE,PARM=NEW
//SYSPRINT DD  SYSOUT=E
//SYSUT2   DD  DSNAME=ps-dsname,
//             DISP=(NEW,CATLG,DELETE),SPACE=(TRK,(5,2)),
//             DCB=(RECFM=FB,LRECL=80,DSORG=PS)
//SYSIN    DD  DATA
./        ADD   LIST=ALL
line 1
line 2
./      ENDUP
/*
