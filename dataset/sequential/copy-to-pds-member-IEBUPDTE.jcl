//JOB01    JOB (),'COPY TO MEMBER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY A PS TO MEMBER
//* **************************************************************
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=ps-dsname,DISP=SHR
//SYSUT2   DD DSN=pds-dsname,DISP=(,CATLG),UNIT=SYSDA,
//         SPACE=(TRK,(5,5,3)),DCB=(RECFM=FB,LRECL=80,BLKSIZE=4080)
//SYSIN    DD *
./      REPRO NEW=PO,MEMBER=member
/*
