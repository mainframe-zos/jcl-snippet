//JOB01    JOB (),'PRINT',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* PRINT A UNIX FILE
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD PATH='/my/filename',
//         FILEDATA=TEXT,
//         RECFM=FB,BLKSIZE=27920,LRECL=80
//SYSUT2   DD SYSOUT=*
//SYSIN    DD DUMMY
