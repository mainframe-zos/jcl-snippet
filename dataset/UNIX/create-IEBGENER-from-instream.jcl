//JOB01    JOB (),'CREATE INSTEAM',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A UNIX FILE WITH INSTREAM
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD *
line 01
line 02
/*
//SYSUT2   DD PATH='/my/filename',
//            PATHOPTS=(OWRONLY,OCREAT,OEXCL),
//            PATHMODE=SIRWXU,
//            PATHDISP=KEEP
//            FILEDATA=TEXT
//SYSIN    DD DUMMY
//SYSPRINT DD SYSOUT=*
