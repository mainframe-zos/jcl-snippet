//JOB01    JOB (),'DELETE',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* DELETE A UNIX FILE
//* **************************************************************
//STEP01   EXEC PGM=IEFBR14
//DD1      DD PATH='/my/filename',
//         PATHDISP=(DELETE,DELETE)
