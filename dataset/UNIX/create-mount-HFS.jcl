//JOB01    JOB (),'CREATE MOUNT HFS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE AND MOUNT A HFS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
  DELETE hfs-dsname
  SET MAXCC = 0
/*
//STEP02   EXEC PGM=IEFBR14
//HFS      DD DISP=(NEW,CATLG),DSN=hfs-dsname,
//            UNIT=3390,SPACE=(CYL,(600,100)),
//            DCB=DSORG=PO,DSNTYPE=HFS
//*
//STEP03   EXEC PGM=IKJEFT01,COND=(0,NE)
//SYSTSPRT DD SYSOUT=*
//SYSTSIN  DD *
  MOUNT FILESYSTEM('hfs-dsname')                -
        MOUNTPOINT('/my/dir/mountpoint')                  -
        TYPE(HFS)                                     -
        MODE(RDWR)
/*
//
