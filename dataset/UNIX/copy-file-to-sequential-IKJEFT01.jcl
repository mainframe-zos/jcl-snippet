//JOB01    JOB (),'COPY FILE TO SEQ',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* COPY A UNIX FILE TO DATASET SEQUENTIAL
//* **************************************************************
//STEP01   EXEC PGM=IKJEFT01
//INHFS    DD   PATH='/my/file',
//              PATHOPTS=(ORDONLY)
//OUTMVS   DD   DSN=ps-dsname,
//              SPACE=(TRK,(5,2)),
//              DCB=(DSORG=PS,RECFM=FB,LRECL=80),
//              DISP=(NEW,CATLG,DELETE)
//SYSTSPRT DD   SYSOUT=*
//SYSTSIN  DD   *
OCOPY INDD(INHFS) OUTDD(OUTMVS) TEXT CONVERT(YES) PATHOPTS(USE)
/*
