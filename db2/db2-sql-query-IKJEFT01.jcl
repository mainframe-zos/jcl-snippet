//JOB01    JOB (),'SQL QUERY DSNTIAUL',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* SQL QUERY - DSNTIAUL
//* **************************************************************
//STEP01   EXEC PGM=EQQDELDS
//SYSPRINT DD SYSOUT=*
//* **************************************************************
//STEP02   EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSTSPRT DD   SYSOUT=*
//SYSTSIN  DD   *
DSN SYSTEM(ssid)
RUN  PROGRAM(DSNTIAUL) PLAN(DSNTIAUL) -
     LIB('hlq.RUNLIB.LOAD') -
PARM('SQL')
END
/*
//SYSPRINT DD   SYSOUT=*
//SYSUDUMP DD   DUMMY
//*
//SYSREC00 DD   DSN=ps-dsname-output,
//              DISP=(NEW,CATLG,DELETE),
//              LIKE=ps-dsname-like
//SYSPUNCH DD   DUMMY,
//              DCB=(LRECL=135,BLKSIZE=1485,RECFM=FB)
//SYSIN    DD   *
SELECT * FROM my.table;
/*
