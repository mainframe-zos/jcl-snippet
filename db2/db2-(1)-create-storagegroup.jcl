//JOB01    JOB (),'CREATE STORAGEGROUP',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* DB2 CREATE STORAGEGROUP
//* **************************************************************
//STEP01   EXEC PGM=IKJEFT01,DYNAMNBR=20,COND=(4,LT)
//SYSTSIN  DD *
DSN SYSTEM(ssid)
RUN  PROGRAM(DSNTIAD) PLAN(DSNTIA12) -
     LIB('hlq.RUNLIB.LOAD')
END
/**
//SYSTSPRT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSPUNCH DD DUMMY
//SYSIN    DD  *
 SET CURRENT SQLID = 'sqlid';
     CREATE STOGROUP storagegroup-name
           VOLUMES
           ("*"
           )
     VCAT ssid;
