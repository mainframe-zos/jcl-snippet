//JOB01    JOB (),'CREATE DATABASE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* DB2 CREATE DATABASE
//* **************************************************************
//STEP01   EXEC PGM=IKJEFT01,DYNAMNBR=20,COND=(4,LT)
//SYSTSIN  DD *
DSN SYSTEM(ssid)
RUN  PROGRAM(DSNTIAD) PLAN(DSNTIA12) -
     LIB('hlq.RUNLIB.LOAD')
END
/**
//SYSTSPRT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD  *
 SET CURRENT SQLID = 'sqlid';
     CREATE DATABASE database-name
          BUFFERPOOL BP1
          INDEXBP    BP2
          STOGROUP storagegroup-name
          CCSID EBCDIC;
    COMMIT;
