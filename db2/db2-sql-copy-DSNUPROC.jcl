//JOB01    JOB (),'FULL COPY TABLESPACE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* FULL COPY TABLESPACE
//* **************************************************************
//STEP01   EXEC PGM=EQQDELDS
//SYSPRINT DD SYSOUT=*
//* **************************************************************
//STEP02   EXEC DSNUPROC,UID='uid',UTPROC='',SYSTEM='ssid'
//SYSCOPY  DD   DSN=ps-dsname.COPY,UNIT=SYSDA,
//              SPACE=(CYL,(15,1)),DISP=(NEW,CATLG,CATLG)
//SYSOUT   DD   SYSOUT=*
//SYSIN    DD   *
COPY TABLESPACE database-name.tablespace-name
/*
