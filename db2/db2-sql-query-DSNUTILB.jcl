//JOB01    JOB (),'SQL QUERY DELETE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* SQL QUERY - DELETE
//* **************************************************************
//STEP01   EXEC PGM=DSNUTILB,PARM='ssid,uid'
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD *
 EXEC SQL
     DELETE FROM uid.table-name
 ENDEXEC
/*


//* **************************************************************
//* **************************************************************


//JOB01    JOB (),'SQL QUERY INSERT',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* SQL QUERY - INSERT
//* **************************************************************
//STEP01   EXEC PGM=DSNUTILB,PARM='ssid,uid'
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD *
 EXEC SQL
     INSERT INTO uid.table-name
         VALUES(intger-value
               ,'char-value'
               ,...)
 ENDEXEC
/*
