//JOB01    JOB (),'UNLOAD TO DATASET',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* UNLOAD TO DATASET
//* **************************************************************
//STEP01   EXEC DSNUPROC,UID='uid',UTPROC='',SYSTEM='ssid'
//SYSREC   DD   DSN=ps-dsname.SYSREC,
//              DISP=(NEW,CATLG,CATLG),
//              UNIT=SYSDA,SPACE=(TRK,(2,1))
//SYSPUNCH DD   DSN=ps-dsname.SYSPUNCH,
//              DISP=(NEW,CATLG,CATLG),
//              UNIT=SYSDA,SPACE=(TRK,(1,1))
//SYSPRINT DD   SYSOUT=*
//SYSIN    DD   *
  UNLOAD TABLESPACE database-name.tablespace-name
    FROM TABLE uid.table-name
