//JOB01    JOB (),'LOAD FROM DATASET',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* LOAD FROM DATASET
//* **************************************************************
//STEP01   EXEC DSNUPROC,UID='uid',UTPROC='',SYSTEM='ssid'
//DSNTRACE DD   SYSOUT=*
//SORTLIB  DD   DSN=SYS1.SORTLIB,DISP=SHR
//SORTWK01 DD   UNIT=SYSDA,SPACE=(4000,(20,20),,,ROUND)
//SORTWK02 DD   UNIT=SYSDA,SPACE=(4000,(20,20),,,ROUND)
//SORTWK03 DD   UNIT=SYSDA,SPACE=(4000,(20,20),,,ROUND)
//SORTWK04 DD   UNIT=SYSDA,SPACE=(4000,(20,20),,,ROUND)
//SORTOUT  DD   UNIT=SYSDA,SPACE=(4000,(20,20),,,ROUND)
//SYSREC   DD   DSN=ps-dsname.SYSREC,
//              DISP=SHR
//SYSREC00 DD   DSN=ps-dsname.SYSREC,
//              DISP=SHR
//SYSUT1   DD   UNIT=SYSDA,SPACE=(4000,(20,20),,,ROUND)
//SYSIN    DD   DSN=ps-dsname.SYSPUNCH,DISP=SHR
//SYSMAP   DD   DSN=ps-dsname.SYSMAP,
//              DISP=(MOD,DELETE,CATLG),
//              SPACE=(8192,(5,5),RLSE),
//              UNIT=SYSDA
