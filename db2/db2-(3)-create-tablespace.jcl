//JOB01    JOB (),'CREATE TABLESPACE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* DB2 CREATE TABLESPACE
//* **************************************************************
//STEP01   EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSTSPRT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSTSIN  DD *
DSN SYSTEM(ssid)
RUN  PROGRAM(DSNTIAD) PLAN(DSNTIA12) -
     LIB('hlq.RUNLIB.LOAD')
END
/*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD  *
 SET CURRENT SQLID = 'sqlid';
      CREATE TABLESPACE tablespace-name
           IN database-name
           USING STOGROUP storagegroup-name
               PRIQTY -1
               SECQTY -1
               ERASE NO
           BUFFERPOOL BP1
           DSSIZE 4G
           CLOSE YES
           LOCKMAX SYSTEM
           SEGSIZE 32
           DEFINE YES
           LOGGED
           LOCKSIZE ANY
           MAXROWS 255
           CCSID EBCDIC
           MAXPARTITIONS 20;
    COMMIT;
