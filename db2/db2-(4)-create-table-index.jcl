//JOB01    JOB (),'CREATE TABLE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* DB2 CREATE TABLE AND INDEX
//* **************************************************************
//STEP01   EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSTSPRT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSTSIN  DD *
DSN SYSTEM(ssid)
RUN  PROGRAM(DSNTIAD) PLAN(DSNTIA12) -
     LIB('hlq.RUNLIB.LOAD')
END
/*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD  *
 SET CURRENT SQLID = 'sqlid';

    CREATE TABLE table-name
        (PK                   VARCHAR(50)      NOT NULL
        ,FK                   VARCHAR(7)       NOT NULL
        ,DESCRIPTION          VARCHAR(100)     NOT NULL
        ,DATE                 DATE             NOT NULL
        ,CONSTRAINT PRIMARYPK PRIMARY KEY
        (PK
        )
        ,CONSTRAINT NAMEFK FOREIGN KEY
        (FK
        )
        REFERENCES sqlid.table (col)
        ON DELETE CASCADE
        )
        IN database-name.tablespace-name
   APPEND NO
   NOT VOLATILE CARDINALITY
   DATA CAPTURE NONE
   AUDIT NONE
   CCSID EBCDIC;

   CREATE UNIQUE INDEX index-name
          ON table-name
          (col-name ASC
          )
          INCLUDE NULL KEYS
          CLUSTER
          DEFINE YES
          COMPRESS NO
          BUFFERPOOL BP2
          CLOSE YES
          DEFER NO
          COPY NO
          USING STOGROUP storagegroup-name
               PRIQTY -1
               SECQTY -1
               ERASE NO
          FREEPAGE 10
          PCTFREE 20
          GBPCACHE CHANGED
          PIECESIZE 4194304K;
/*


//* **************************************************************
//* **************************************************************


//JOB01    JOB (),'CREATE TABLE LIKE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE TABLE LIKE
//* **************************************************************
//STEP01   EXEC PGM=DSNUTILB,PARM='ssid,uid'
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 EXEC SQL
     CREATE TABLE uid.table-dest-name LIKE uid.table-source-name
     IN database-name.tablespace-name table
 ENDEXEC
/*
