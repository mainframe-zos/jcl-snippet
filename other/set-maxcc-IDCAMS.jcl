//JOB01    JOB (),'SET MAXCC',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* SET MAXCC OF STEPS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 SET MAXCC = 4
/*
//STEP02   EXEC PGM=IDCAMS,COND=(0,NE,CK1)
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 SET MAXCC = 0
/*
//STEP03   EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 SET MAXCC = 8
/*
//STEP04   EXEC PGM=IDCAMS,COND=(4,LE)
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 SET MAXCC = 0
/*
