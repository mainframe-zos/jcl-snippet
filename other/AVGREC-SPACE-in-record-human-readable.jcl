//JOB01    JOB (),'SIZE IN RECORDS',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* CREATE A PS IN O OR KO OR MO
//* **************************************************************
//STEP02   EXEC PGM=IEFBR14
//DD1      DD DSN=dsname,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(RECFM=FB,LRECL=80),
//            SPACE=(2,(5,2)),AVGREC=M
//*

//* Use the AVGREC parameter when you define a new data set to specify that:
//*    The units of allocation requested for storage space are records.
//*    The primary and secondary space quantity specified on the SPACE parameter represents units, thousands, or millions of records.
//*
//* AVGREC=U to specify the record count in 1 record
//* AVGREC=K to specify the record count in K (1024 records)
//* AVGREC=M to specify the record count in M (1024*1024=1,048,576 records) A
//*
//* SPACE=(n,(x,y))
//*    n = record length
//*    x = primary quantity
//*    y = a secondary quantity
//*
//* exemple :
//*   SPACE=(2,(5,2)),AVGREC=M
//*     indicate an average record length of 2 bytes,
//*     a primary quantity of 10 Mo,
//*     and a secondary quantity of 5 Mo.
