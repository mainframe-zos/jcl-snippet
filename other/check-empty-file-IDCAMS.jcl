//JOB01    JOB (),'CHECK EMPTY FILE',
//         CLASS=E,MSGCLASS=E,MSGLEVEL=(1,1),
//         NOTIFY=&SYSUID
//* **************************************************************
//* IF EMPTY FILE ?
//* IF RC = 4 ===> EMPTY FILE
//* SYSPRINT DD  DSN= ===> CANCEL FILE CREATION
//*                        PRINT IS IMPRESSION USELESS
//* **************************************************************
//STEP01   EXEC PGM=IDCAMS,REGION=0M
//DDIN     DD  DSN=my.ps,DISP=SHR
//SYSPRINT DD  DSN=
//SYSIN    DD  *
 PRINT INFILE(DDIN)-
       COUNT(1)
/*
