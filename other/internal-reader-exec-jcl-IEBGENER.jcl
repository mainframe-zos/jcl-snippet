//JOB01    JOB (),'SUBMIT JCL INTERNAL READER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* SUBLIT PROGRAM THROUGH INTERNAL READER.
//* **************************************************************
//STEP01   EXEC PGM=IEBGENER
//SYSUT2   DD SYSOUT=(*,INTRDR)
//SYSIN    DUMMY
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DATA,DLM='$$'
//JOB01    JOB (),'JOB SUBMIT WITH INTERNAL READER',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//*         THIS JOB IS SUBMIT WITH INTERNAL READER
//* **************************************************************
// your jcl code
$$
/*
