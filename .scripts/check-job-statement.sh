#!/usr/bin/env bash

rc=0
ci_flag="/tmp/.job-state-ko.flag"
file_pattern="*.jcl"
# file_pattern="set-maxcc-IDCAMS.jcl"

function msg_and_rc {
  local lfile="$1"
  local lmsg=$2
  local lrc="$3"

  if [[ ${lrc} -ne 0 ]]; then
    echo "[ERRO] (${lrc}) $lfile ${lmsg}"
    touch "${ci_flag}"
  fi
}

echo -e "Check JOB STATEMENT, regexp :"
echo "    ^//JOB01    JOB (),'\([A-Z]\|[[:space:]]\|[0-9]\)*',$"
echo "    ^//         CLASS=E,MSGCLASS=E,"
echo "    ^//         NOTIFY=&SYSUID$"
echo ""

[[ -f "${ci_flag}" ]] && rm "${ci_flag}"

find . -name "${file_pattern}" | sort | while read file; do
  grep -n "^//JOB" "${file}" | while read line; do
    jobline1=$(echo "${line}" | awk -F\: '{print $1}')
    jobline2=$(( ${jobline1} + 1 ))
    jobline3=$(( ${jobline1} + 2 ))

    jobvalue1=$(echo "${line}" | awk -F\: '{print $2}')
    jobvalue2=$(sed -n "${jobline2}p" "${file}")
    jobvalue3=$(sed -n "${jobline3}p" "${file}")

    echo "${jobvalue1}" | grep -q "^//JOB01    JOB (),'\([A-Z]\|[[:space:]]\|[0-9]\)*',$"
    msg_and_rc "${file}" "[${jobline1}] ${jobvalue1}" $?
    echo "${jobvalue2}" | grep -q "^//         CLASS=E,MSGCLASS=E,"
    msg_and_rc "${file}" "[${jobline2}] ${jobvalue2}" $?
    echo "${jobvalue3}" | grep -q "^//         NOTIFY=\&SYSUID$"
    msg_and_rc "${file}" "[${jobline3}] ${jobvalue3}" $?

  done
done

if [[ -f "${ci_flag}" ]]; then
  rc=1
else
  echo "No error found"
fi

exit ${rc}
