#!/usr/bin/env bash

rc=0
ci_flag="/tmp/.exec-state-ko.flag"
file_pattern="*.jcl"
# file_pattern="db2-load-DSNUPROC.jcl"

function msg_and_rc() {
  local lfile="$1"
  local lmsg="$2"
  local lrc="$3"

  if [[ ${lrc} -ne 0 ]]; then
    echo "[ERRO] (${lrc}) ${lfile} ${lmsg}"
    touch "${ci_flag}"
  fi
}

echo "Check EXEC STATEMENT, regexp :"
echo "    ^//STEP[0-9]{2}   EXEC "
echo ""

[[ -f "${ci_flag}" ]] && rm "${ci_flag}"

find . -name "${file_pattern}" | sort | while read file; do
  cat "${file}" | grep -nw "//.*EXEC" | while read line; do

    ddline=$(echo "${line}" | awk -F\: '{print $1}')
    ddvalue=$(echo "${line}" | awk -F\: '{print $2}')

    [[ "${ddvalue}" =~ ^//\* ]] && continue

    echo "${ddvalue}" | grep -q "^//STEP[0-9]\{2\}   EXEC "
    msg_and_rc "${file}" "[${ddline}] ${ddvalue}" $?

  done
done

if [[ -f "${ci_flag}" ]]; then
  rc=1
else
  echo "No error found"
fi

exit ${rc}
