//JOB01    JOB (),'EXECUTE CICS COMMAND',
//         CLASS=E,MSGCLASS=E,
//         NOTIFY=&SYSUID
//* **************************************************************
//* JOB USED TO SUBMIT CICS COMMANDS
//* **************************************************************
//STEP01   EXEC PGM=IEFBR14
// F CICSPRD,'CEMT INQ TER'
// F CICSPRD,'CEMT INQ TAS'
// F CICSPRD,'CEMT SET TER(A110) ACQ'
//
