# jcl-snippet

Some powerfull JCL snippet for better life.
A library of useful JCL that i frequently use.
Feel free to make use of them

# Tips
## EQQDELDS
EQQDELDS deletes any data set that has a disposition of (NEW,CATLG), or (NEW,KEEP) for SMS, if the data set is already present in the catalog
```
//STEP01   EXEC PGM=EQQDELDS
//SYSPRINT DD SYSOUT=*
```
## JCLLIB (PROCEDURE)
How-to write a JOBLIB
```
//JOB01    JOB (),'CATLG',CLASS=E,MSGCLASS=E,NOTIFY=&SYSUID
//MYLIBS1  JCLLIB  ORDER='MY.LIB.JCL.PROCEDURE'
//* STEP1 CALL PROCEDURE WITH A VARIABLE(MEMBER NAME)
//STEP1    EXEC PROC=PROC1,VARIABLE1=VALUE
```
JCL procedure member : MY.LIB.JCL.PROCEDURE(PROC1)
```
//MYPROC   PROC VARIABLE1=
//MYEXP    EXPORT SYMLIST=*
//MYVAR1   SET VARIABLE1=&VARIABLE1.
//*
//STEP01   EXEC PGM=......
do something
//         PEND
```

## JOBLIB
How-to write a JOBLIB
```
//JOB01    JOB (),'JOBLIB',CLASS=E,
//            MSGCLASS=E,NOTIFY=&SYSUID
//*
//JOBLIB   DD DSN=dsname,DISP=OLD
//*
//STEP1    EXEC MYPROC,VAR1=VALUE1,VAR2=VALUE2
//PROCSTEP.SYSUT1 DD DSN=dsname
//PROCSTEP.SYSUT2  DD SYSOUT=E
```



## refback
How to use a refback
```
//STEP2    EXEC PGM=IEFBR14
//DD1      DD DISP=(MOD,DELETE,DELETE),DSN=*.STEP1.DD1
```

## variable
How-to declare and use a variable
```
//DECLARE  SET HLQ=&SYSUID
...
//USAGE    DD DSN=&HLQ.MY.DATASET
```

### system symbol
How-to use system symbol
```
//MYEXP    EXPORT SYMLIST=(HLVL)
//MYVAR1   SET HLVL='MYHLVL.ISKJCL'
//*
//STEP1    EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *,SYMBOLS=(JCLONLY)
 DELETE &HLVL..EMP.REPORT
```

# Website
A list of website with exemple and explanation
- https://www.mainframestechhelp.com/
- https://www.tutorialspoint.com/vsam/
